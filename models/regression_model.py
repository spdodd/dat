import random
import csv
from decimal import Decimal

class VectorModel(object):
    def __init__(self, subtasks, model_def_file):
        self.required = ['touch', 'hear', 'see', 'look,''drive']
        self.subtasks = subtasks
        self.model_def_file = model_def_file

    def check(self):
        """
        Check that provided subtasks contain only
        needed information to run the model
        (required fields defined as self.required)
        """
        for subtask in self.subtasks:
            for action in subtask.actions:
                if (action not in self.required):
                    return False


    def load_def_file(self):
        self.traps_dict = {}
        self.sapt_dict = {}
        with open(self.model_def_file) as f:
            reader = csv.DictReader(f)
            header = reader.fieldnames
            dv_index = header.index('dv')
            demand_index = header.index('demand')
            coef_index = header.index('coef')
            for row in reader:
                if row['dv'] == 'Traps':
                    self.traps_dict[row['demand'].lower()] = Decimal(row['coef'])
                elif row['dv'] == 'Sapt':
                    self.sapt_dict[row['demand'].lower()] = Decimal(row['coef'])


    def calc_traps_sapt(self, subtask, val_dict):
        #print val_dict
        val = val_dict['intercept']
        for action in subtask.actions:
            if action != 'drive':
                val += val_dict[action.lower()]
        return val


    def run(self):
        """
        Vector model logic goes here:
        ------------------------------------
        sample w/ randomly generated outputs
        """
        self.load_def_file()
        new_subtasks = []
        for subtask in self.subtasks:
            traps = self.calc_traps_sapt(subtask, self.traps_dict)
            sapt = self.calc_traps_sapt(subtask, self.sapt_dict)
            subtask.transition = float(traps)
            subtask.dwell = float(traps) * float(sapt) * float(subtask.end_ts - subtask.start_ts)
            new_subtasks.append(subtask)
        return new_subtasks
