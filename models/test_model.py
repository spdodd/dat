import random

class VectorModel(object):
    def __init__(self, subtasks):
        self.required = ['touch', 'hear', 'see', 'drive']
        self.subtasks = subtasks

    def check(self):
        """
        Check that provided subtasks contain only
        needed information to run the model
        (required fields defined as self.required)
        """
        for subtask in self.subtasks:
            for action in subtask.actions:
                if (action not in self.required):
                    return False

    def run(self):
        """
        Vector model logic goes here:
        ------------------------------------
        sample w/ randomly generated outputs
        """
        new_subtasks = []
        for subtask in self.subtasks:
            subtask.est_dwell = random.random()
            subtask.est_transition = random.random()
            new_subtasks.append(subtask)
        return new_subtasks
