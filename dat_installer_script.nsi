Outfile "install_ahead_dat.exe"

InstallDir "$PROGRAMFILES\DAT"

Section

SetOutPath $INSTDIR

File /r "*"

CreateDirectory "$PROFILE\DAT"
CreateDirectory "$PROFILE\DAT\subtask structure files"
CreateDirectory "$PROFILE\DAT\vector models"
CreateDirectory "$PROFILE\DAT\flag files"
CopyFiles "$INSTDIR\subtask structure files\*" "$PROFILE\DAT\subtask structure files"
CopyFiles "$INSTDIR\vector models\*" "$PROFILE\DAT\vector models"
CopyFiles "$INSTDIR\flag files\*" "$PROFILE\DAT\flag files"
CreateShortCut "$DESKTOP\DAT.lnk" "$INSTDIR\DAT.exe"

SectionEnd