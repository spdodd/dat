#!/usr/bin/env python2
import os
import sys
import datetime as dt
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager
import matplotlib.dates
import matplotlib.patches as mpatches
from matplotlib.dates import WEEKLY,MONTHLY, DateFormatter, rrulewrapper, RRuleLocator
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib.transforms import Bbox
import numpy as np
import csv
import simplejson as json
from decimal import *
from random import shuffle
import argparse
import copy
import zipfile
import codecs
import pickle
import math
reload(sys)
sys.setdefaultencoding('utf-8')
HOME_DIR = os.path.expanduser("~")
DAT_DIR = os.path.join(HOME_DIR, 'DAT')
PLT_DPI = 100
TBL_WIDTH_SEC = 15
MAX_L_WIDTH = 0.333333
COLOR_LIST = matplotlib.colors.BASE_COLORS.keys() + matplotlib.colors.CSS4_COLORS.keys()
DEFAULT_COLOR = 'black'

class SubTask(object):
    def __init__(self, index, name, actions, start_ts, end_ts, duration):
        self.index = index
        self.name = name
        self.actions = actions
        self.start_ts = start_ts
        self.end_ts = end_ts
        self.duration = duration


class Streak(object):
    def __init__(self, stype, start_ts, end_ts, rank, show_ptar = False, ptar=None, ptar_color='black'):
        self.stype = stype
        self.start_ts = start_ts
        self.end_ts = end_ts
        self.rank = rank
        self.show_ptar = show_ptar
        self.ptar = ptar
        self.ptar_color = ptar_color
        if self.ptar_color is None:
            self.ptar_color = 'black'

def pxl2inch(size_pxl):
    return float(size_pxl) / math.sqrt(float(PLT_DPI))

def load_config(config_file):
    with open(config_file) as f:
        data = json.load(f)
    colors = data['DAT']['colors']
    print colors
    flags = data['DAT']['flags']
    if 'targets' in data['DAT'].keys():
        targets = data['DAT']['targets']
        return colors, flags, targets
    else:
        return colors, flags

def load_vector_model(model_file):
    mod_dir, model = os.path.split(model_file)
    mod_dir = os.path.abspath(mod_dir)
    vm_name = os.path.splitext(model)[0]
    path = list(sys.path)
    sys.path.insert(0, mod_dir)
    vm = None
    try:
        vm = __import__(vm_name)
    finally:
        sys.path[:] = path
    return vm


def get_actions(non_action_items, header):
    actions = []
    for item in header:
        if item not in non_action_items:
            actions.append(item.lower())
    return actions

def get_encode_type(in_file):
    encodings = ['cp1252', 'windows-1252', 'ascii']
    for e in encodings:
        try:
            print e
            fh = codecs.open(in_file, 'r', encoding=e)
        except UnicodeDecodeError:
            pass
        else:
            return e

def load_subtasks(subtask_file):
    subtasks = []
    curr_time = float('0.000')
    encoding = get_encode_type(subtask_file)
    print encoding
    with codecs.open(subtask_file, 'r', encoding=encoding) as f:
        reader = csv.DictReader(f, dialect='excel')
        header = reader.fieldnames
        non_action_items = ['salience','difficulty','interest','duration','subtask','display', 'latency']
        action_labels = get_actions(non_action_items, header)
        #print action_labels
        i = 0
        for row in reader:
            #yield {unicode(key, 'utf-8'): unicode(value, 'utf-8') for key, value in row.iteritems()}
            #yield [unicode(cell, 'utf-8') for cell in row]
            #for cell in row:
            #    row[cell] = unicode(cell, 'utf-8')
            performed_actions = []
            for action in action_labels:
                if row[action] == '1':
                    performed_actions.append(action)

            runtime = float(row['duration'])
            subtask = SubTask(i, str(row['subtask'].encode('utf-8')), performed_actions, curr_time, curr_time + runtime, runtime)
            #print subtask.name
            subtasks.append(subtask)
            curr_time += runtime
            i += 1
    return action_labels, subtasks


def get_streaks(subtasks, streak_type, flags):
    attr = flags[streak_type]['value'].lower()
    flagged_subtasks = get_flagged_subtasks(subtasks, attr, streak_type, flags)

    streaks = []

    i = 0
    for subtask in flagged_subtasks:
        ptar = None
        ptar_color = None
        show_ptar = False
        if 'show_ptar' in flags[streak_type].keys():
            show_ptar = flags[streak_type]['show_ptar'] == 1
            if show_ptar:
                ptar = format(float(subtask.ptar), '0.2f')
                if 'ptar_color' in flags[streak_type].keys():
                    ptar_color = flags[streak_type]['ptar_color']
        streaks.append(Streak(streak_type, subtask.start_ts, subtask.end_ts, i, show_ptar=show_ptar, ptar=ptar, ptar_color=ptar_color))
        i += 1

    return streaks

def order_subtasks(subtasks, attr, order='asc'):
    if (order == 'asc'):
        reverse = False
    elif (order == 'desc'):
        reverse = True

    if (attr == 'index'):
        return sorted(subtasks, key=lambda x: getattr(x, attr), reverse=reverse)
    elif (attr == 'dwell'):
        return sorted(subtasks, key=lambda x: x.dwell, reverse=reverse)
    elif (attr == 'transition'):
        return sorted(subtasks, key=lambda x: x.transition, reverse=reverse)
    elif (attr == 'duration'):
        return sorted(subtasks, key= lambda x: (x.end_ts - x.start_ts), reverse=reverse)
    elif (attr == 'ptar'):
        return sorted(subtasks, key=lambda x: x.ptar, reverse=reverse)
    elif (attr == 'tti'):
        return sorted(subtasks, key=lambda x: x.tti, reverse=reverse)

def get_flagged_subtasks(subtasks, attr, streak_type, flags):
    num_flagged = int(flags[streak_type]['num_flagged'])
    classifier = flags[streak_type]['classifier']

    if (classifier == 'MAX'):
        flagged_subtasks = order_subtasks(subtasks, attr, 'desc')[:num_flagged]
    elif (classifier == 'MIN'):
        flagged_subtasks = order_subtasks(subtasks, attr, 'asc')[:num_flagged]

    return flagged_subtasks


def create_proxy(label):
    line = matplotlib.lines.Line2D([0], [0], linestyle='none', mfc='black',
                                   mec='none', marker=r'$\mathregular{{{}}}$'.format(label))
    return line

def make_plot(actions, subtasks, streaks, colors, plotfile, ttl_targets=None, ttl_in_targets=None):
    bar_color = colors['default_bar']
    warm_colors = colors['warm_streaks']
    hot_colors = colors['hot_spots']
    trans = float(colors['transparency'])

    warm_index = len(actions)
    hot_index = warm_index + 1
    y_labels = copy.deepcopy(actions)
    y_labels.append('warm streaks')
    y_labels.append('hot spots')

    p_space = 0.5 #plot spacing factor
    bar_height = 0.42

    ilen=len(y_labels)
    pos = np.arange(p_space,ilen*p_space+p_space,p_space)
    letter_spaces = [0.02, 0.15, 0.28]
    l_space_idx = 0
    try:
        fig = plt.figure(figsize=((subtasks[len(subtasks) - 1].end_ts / 2.1),4))
        ax = fig.add_subplot(111)
        handles = []
        labels = []
        unicode_char = 65 #A
        n_subtasks = len(subtasks)
        subtask_idx = 0
        for subtask in subtasks:
            if unicode_char > 90:
                l_name = unichr(65) + unichr(64 + unicode_char % 90)
            else:
                l_name = unichr(unicode_char)
            #print l_name
            font_size_scale = 3
            if (l_name.lower() == 'i' or l_name.lower() == 'j'):
                font_size_scale = 1
            t = matplotlib.textpath.TextPath((0,0), unichr(65), size=font_size_scale)
            bb = t.get_extents()
            #print bb.width
            label_width = pxl2inch(bb.width)
            #print label_width
            #print len(l_name)
            if len(l_name) > 1:
                fontsize = 7
            else:
                fontsize = 9
            subtask_label = ax.text(subtask.end_ts - ((subtask.end_ts - subtask.start_ts + label_width) / 2), letter_spaces[l_space_idx], l_name, fontsize=fontsize)
            if subtask_idx < (len(subtasks) - 1):
                time_diff = abs(float(subtask.start_ts) - float(subtasks[subtask_idx + 1].end_ts))
                if time_diff < MAX_L_WIDTH:
                    l_space_idx = (l_space_idx + 1) % len(letter_spaces)
                else:
                    l_space_idx = 0
            else:
                l_space_idx = 0
            labels.append(subtask.name)
            handles.append(l_name)
            unicode_char += 1
            plt.axvline(x=subtask.end_ts, linestyle='--', alpha=0.2, color='grey')
            subtask_idx += 1
            for action in subtask.actions:
                i = y_labels.index(action)
                ax.barh(float((i*p_space)+p_space), float(subtask.end_ts - subtask.start_ts), left=float(subtask.start_ts),
                        height=float(bar_height),align='center', edgecolor=bar_color, color=bar_color, alpha=trans)
    except Exception as e:
        return "badsubtaskfile"
    #print i
    if i == 1:
        i = y_labels.index('warm streaks') - 1
    plt.axhline(y=i-0.75, color='black', linestyle='-')
    for streak in streaks:
        if (streak.stype == 'warm'):
            color = warm_colors[streak.rank]
            i = warm_index
        else:
            color = hot_colors[streak.rank]
            i = hot_index
        ax.barh((i*p_space)+p_space, streak.end_ts - streak.start_ts, left=streak.start_ts,
                height=bar_height,align='center', edgecolor=color, color=color, alpha=trans)
        if 'show_ptar' in vars(streak).keys():
            ptar_color = streak.ptar_color
            if streak.show_ptar:
                h_font_offset = -0.4
                v_font_offset = 0.07
                ptar = streak.ptar
                streak_center = streak.start_ts + ((streak.end_ts - streak.start_ts) / 2)
                print ptar_color
                ax.text(streak_center + h_font_offset, i*p_space+p_space + v_font_offset, ptar, fontsize=10, color=ptar_color)

    locsy, labelsy = plt.yticks(pos,y_labels)
    plt.setp(labelsy, fontsize = 10)
    ax.set_ylim(ymin = -0.1, ymax = ilen*p_space+p_space)
    ax.margins(x=0)
    font = font_manager.FontProperties(size='small')
    ax.invert_yaxis()
    proxies = [create_proxy(item) for item in handles]
    cols = ['Label', 'Value']
    if ttl_targets is not None:
        label = 'TTL_TRAPS'
        value = format(float(subtasks[0].ttl_traps), '0.6f')
        rows = [[label, value]]
        for ttl_name in ttl_targets.keys():
            ttl_parts = ttl_name.split(' ')
            ttl_parts.pop(0)
            ttl_name_new = ' '.join(['Target'] + ttl_parts)
            if len(ttl_name_new) > 33:
                ttl_name_new = ttl_name_new[0:30] + '...'
            ttl_in_name = ' '.join(['In_Target'] + ttl_parts)
            if len(ttl_in_name) > 33:
                ttl_in_name = ttl_in_name[0:30] + '...'
            rows.append([ttl_name_new, format(float(ttl_targets[ttl_name]), '0.6f')])
            rows.append([ttl_in_name, int(ttl_in_targets[ttl_name])])
        row_labels = [''] * len(rows)
        bbox = Bbox.from_bounds(0,0,1,1)
        #print bbox
        print len(rows)
        tbl_width = TBL_WIDTH_SEC / subtask.end_ts
        #print tbl_width
        tbl_offset = 1 - tbl_width
        ttl_table = plt.table(cellText=rows,
                              rowLabels=row_labels,
                              colLabels=cols,
                              loc='bottom left',
                              bbox=[tbl_offset,-0.2 - (float(len(rows) / 10.0))
                                    ,tbl_width,0.1 + (float(len(rows)) / 10.0)]
        )
        ttl_table.auto_set_font_size(False)

        #print vars(ttl_table._cells[(3,0)])
        for cell in ttl_table._cells:
            ttl_table._cells[cell]._visible_edges = u'LR'
        #print len(rows)
        ttl_table._cells[(0,0)]._visible_edges = u'TBLR'
        ttl_table._cells[(0,1)]._visible_edges = u'TBLR'
        ttl_table._cells[(len(rows), 0)]._visible_edges = u'BLR'
        ttl_table._cells[(len(rows), 1)]._visible_edges = u'BLR'
        ttl_table.set_fontsize(9)
    leg = ax.legend(proxies, labels, numpoints=1, markerscale=2, bbox_to_anchor=(1.0092,1.01))
    leg.get_frame().set_boxstyle('square')
    leg.get_frame().set_edgecolor('black')
    #print leg.get_frame()
    minorLocator = MultipleLocator(0.1)
    majorLocator = MultipleLocator(1)
    ax.xaxis.set_minor_locator(minorLocator)
    ax.xaxis.set_major_locator(majorLocator)
    ax.tick_params(labeltop='off')
    plt.minorticks_off()
    plt.savefig(plotfile,bbox_inches='tight')
    return fig

def save_dat(out_name):
    print out_name
    temp_dir = os.path.join(HOME_DIR, "dat_temp")
    if not os.path.exists(temp_dir):
        os.mkdir(os.path.join(temp_dir))
    temp_pic = os.path.join(temp_dir, "temp_pic.png")
    plt.savefig(temp_pic)
    with zipfile.ZipFile(out_name, 'w') as myzip:
        myzip.write(temp_pic)

if __name__=="__main__":
    c_bin, subtask_file, cfg_file, vm_file, out_file = sys.argv
    actions, subtasks = load_subtasks(subtask_file)
    colors, flags = load_config(cfg_file)
    vm = load_vector_model(vm_file)
    my_model = vm.VectorModel(subtasks, os.path.abspath('model_defs.csv'))
    my_model.run()


    warm_streaks = get_streaks(subtasks, 'warm', flags)
    hot_streaks = get_streaks(subtasks, 'hot', flags)
    streaks = hot_streaks + warm_streaks
    make_plot(actions, subtasks, streaks, colors, filename)
    plt.savefig(out_file)
    plt.show()
