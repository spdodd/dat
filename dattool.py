#!/usr/bin/env python2
from PyQt4 import QtGui, QtCore, Qt
import os
import sys
import datgui
import dat
import tempfile
import csv
import zipfile
import copy
import simplejson as json
import shutil
import logging
import datetime
import traceback
from cStringIO import StringIO
from PyQt4.uic import loadUiType
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import (FigureCanvasQTAgg as FigureCanvas, NavigationToolbar2QT as NavigationToolBar)
from dat_error import datError as datErrDialog
reload(sys)

HOME_DIR = os.path.expanduser("~")
DAT_DIR = os.path.join(HOME_DIR, 'DAT')

sys.setdefaultencoding("utf-8")
class CustomScroll(QtGui.QScrollArea):
    def __init__self(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

class CustomLabel(QtGui.QLabel):
    def __init__(self, parent=None, default_name=None):
        QtGui.QLabel.__init__(self, parent)
        self.default_name = default_name
        #QtGui.QWidget.__init__(self, parent)
        self.connect(self, Qt.SIGNAL('triggered()'), self.closeEvent)
        self.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        saveAction = QtGui.QAction("Save Plot", self)
        saveAction.triggered.connect(self.save)
        self.addAction(saveAction)
    def save(self):
        print self.plot_file
        print self.default_name
        if self.plot_file:
            out_png = QtGui.QFileDialog.getSaveFileName(directory=QtCore.QString(os.path.join(DAT_DIR, self.default_name)),
                                                        filter=QtCore.QString('PNG file (*.png)'))
            shutil.copy(self.plot_file, out_png)
    def closeEvent(self, event):
        print "Closing"
        self.destroy

#class PictureBox(QtGui.QWidget):
#    def __init__(self, parent=None):
#        QtGui.QWidget.__init__(self, parent)

class DatApp(QtGui.QMainWindow, datgui.Ui_MainWindow):
    def __init__(self, parent=None):
        super(DatApp, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle('AHEAD DAT')
        self.ModelSelectBtn.clicked.connect(lambda: self.select_file(self.ModelSelectBox, ['Model File (*.model)']))
        self.SubtaskSelectBtn.clicked.connect(lambda: self.select_file(self.SubtaskSelectBox, ['Subtask File (*.csv)']))
        self.ConfigSelectBtn.clicked.connect(lambda: self.select_file(self.ConfigSelectBox, ['Config File (*.json)']))
        #self.DefSelectBtn.clicked.connect(lambda: self.select_file(self.DefSelectBox, ['Model Definition File (*.csv)']))
        self.LoadBtn.clicked.connect(self.open_dat)
        self.RunBtn.clicked.connect(self.show_plot)
        self.SaveBtn.clicked.connect(self.save_file)
        self.graph_created = False
        self.temp_dir = None
        self.error_log = None
        self.plot_file = None
        self.csv_file = None
        self.model_temp_dir = None

    def display_error(self, msg, inst=None):
        #error = QtGui.QErrorMessage()
        #error.showMessage(msg)
        #error.exec_()
        if inst != None:
            print inst
            log_stream = StringIO()
            #logging.basicConfig(stream=log_stream, level=logging.INFO)
            #logging.error(inst, exc_info=True)
            handler = logging.StreamHandler(log_stream)
            log = logging.getLogger('mylogger')
            log.setLevel(logging.INFO)
            for l_handler in log.handlers:
                log.removeHandler(l_handler)
            log.addHandler(handler)
            log.error(inst, exc_info=True)
            handler.flush()

            #log = log_stream.getvalue()
            print log
            msg = "{}\n-------------------------------------\n\n{}".format(msg, log_stream.getvalue())
        error = datErrDialog(self)

        error.errorText.setText(msg)
        error.exec_()

    def log_error(self, inst):
        print 'error'
        log_dir = os.path.join(DAT_DIR, 'ErrorLogs')
        if not os.path.exists(log_dir):
            os.mkdir(log_dir)
        log_file = os.path.join(log_dir, 'log_{}.error'.format(str(datetime.datetime.now().isoformat()).replace(':','-').replace('.','-').replace('T', '-')))
        #log_file = os.path.join(log_dir, 'blah.errlog')
        temp_log = os.path.join(self.temp_dir, 'errors.txt')
        logging.basicConfig(filename=temp_log)
        #with open(temp_log, 'a') as f:
        #    f.write(str(inst))
        logging.error(inst, exc_info=True)
        with zipfile.ZipFile(log_file, 'w') as zf:
            zf.write(temp_log, os.path.basename(temp_log))
            #zf.write(self.model_def_file, os.path.basename(self.model_def_file))
            zf.write(self.subtask_file, os.path.basename(self.subtask_file))
            zf.write(self.config_file, os.path.basename(self.config_file))
            zf.write(self.model_file, os.path.basename(self.model_file))

    def load_model_file(self, model_file):
        self.model_temp_dir = tempfile.mkdtemp()
        with open(model_file, 'rb') as f:
            z = zipfile.ZipFile(f)
            for name in z.namelist():
                z.extract(name, self.model_temp_dir)
                if os.path.splitext(name)[1].lower() == '.py':
                    self.model_logic_file = os.path.join(self.model_temp_dir, name)
                elif os.path.splitext(name)[1].lower() == '.csv':
                    self.model_def_file = os.path.join(self.model_temp_dir, name)


    def load_dat(self, datfile):
        #print datfile
        dat_temp = tempfile.mkdtemp()
        self.temp_dir = dat_temp
        with open(datfile, 'rb') as f:
            z = zipfile.ZipFile(f)
            for name in z.namelist():
                z.extract(name, dat_temp)
        with open(os.path.join(dat_temp, 'metadata.json'), 'rb') as f:
            metadata = json.load(f)
        self.model_file = os.path.join(dat_temp,metadata['model_file'])
        self.load_model_file(self.model_file)
        self.subtask_file = os.path.join(dat_temp,metadata['subtask_file'])
        #self.model_def_file = os.path.join(dat_temp,metadata['model_def_file'])
        self.plot_file = os.path.join(dat_temp,metadata['plot_file'])
        self.results_file = os.path.join(dat_temp,metadata['results_file'])
        #self.csv_file = self.results_file
        #self.SubtaskTable.csv_file = self.results_file
        self.config_file = os.path.join(dat_temp,metadata['config_file'])

        self.SubtaskSelectBox.setText(self.subtask_file)
        self.ModelSelectBox.setText(self.model_file)
        #self.DefSelectBox.setText(self.model_def_file)
        self.ConfigSelectBox.setText(self.config_file)

        self.repopulate_table()
        self.SubtaskTable.csv_file = self.results_file
        self.pixbox = QtGui.QWidget()
        self.pixmap = QtGui.QPixmap(os.path.join(self.plot_file))
        self.pixbox.setWindowTitle("sample plot")
        default_name = os.path.basename(os.path.splitext(self.subtask_file)[0])
        self.label = CustomLabel(self.pixbox, default_name=default_name)
        self.label.plot_file = self.plot_file
        self.label.setPixmap(self.pixmap)
        #self.label = PlotBox(self.plot_file)
        #self.pixmap = self.label.pixmap
        self.scrollArea = CustomScroll()
        self.scrollArea.setBackgroundRole(QtGui.QPalette.Dark)
        self.scrollArea.setWidget(self.label)
        self.scrollArea.resize(self.pixmap.width(), self.pixmap.height())
        self.scrollArea.setWindowTitle("Plot")
        self.scrollArea.show()
        self.graph_created = True
        #pixbox = PlotBox(self.plot_file)


    def repopulate_table(self):
        with open(self.results_file, 'rb') as f:
            lines = f.readlines()
            i = 0
            for line in lines:
                if i == 0:
                    header = line.rstrip('\r\n').split(',')
                    self.SubtaskTable.setColumnCount(len(header))  # change this
                else:
                    self.SubtaskTable.insertRow(i - 1)
                    vals = line.rstrip('\r\n').split(',')
                    j = 0
                    for val in vals:
                        self.SubtaskTable.setItem(i - 1, j, QtGui.QTableWidgetItem(str(val)))
                        j += 1
                i += 1
            self.SubtaskTable.setHorizontalHeaderLabels(header)
            self.SubtaskTable.setVerticalHeaderLabels(self.make_vert_labels(i))
            #self.SubtaskTable.setRowCount(i)
            self.SubtaskTable.horizontalHeader().setResizeMode(QtGui.QHeaderView.ResizeToContents)
            self.SubtaskTable.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)

    def open_dat(self):
        datfile = QtGui.QFileDialog.getOpenFileName(directory=QtCore.QString(DAT_DIR),filter=QtCore.QString('DAT file (*.dat)'))
        self.load_dat(datfile)

    def save_file(self):
        if not self.graph_created:
            self.display_error("No Data Generated! Please run model before saving.")
            return False
        default_name = os.path.basename(os.path.splitext(self.subtask_file)[0])
        out_file = str(QtGui.QFileDialog.getSaveFileName(self, 'Save DAT File', directory=QtCore.QString(os.path.join(DAT_DIR, default_name)),filter='DAT File (*.dat)'))
        metadata = {'model_file':os.path.basename(self.model_file), 'subtask_file':os.path.basename(self.subtask_file),
                    'config_file':os.path.basename(self.config_file),'results_file':os.path.basename(self.csv_file),'plot_file':os.path.basename(self.plot_file)}
        metadata_file = os.path.join(self.temp_dir, 'metadata.json')
        with open(metadata_file, 'w') as f:
            json.dump(metadata, f)
        with zipfile.ZipFile(out_file, 'w') as zf:
            zf.write(self.plot_file, os.path.basename(self.plot_file))
            zf.write(self.csv_file, os.path.basename(self.csv_file))
            #zf.write(self.model_def_file, os.path.basename(self.model_def_file))
            zf.write(self.subtask_file, os.path.basename(self.subtask_file))
            zf.write(self.config_file, os.path.basename(self.config_file))
            zf.write(self.model_file, os.path.basename(self.model_file))
            zf.write(metadata_file, os.path.basename(metadata_file))

    def select_file(self, DisplayBox, file_types):
        #file_dialog = QtGui.QFileDialog(self)
        #file_dialog.setNameFilters(file_types)
        #file_dialog.selectNameFilter(file_types[0])
        #DisplayBox.setText(file_dialog.getOpenFileName())
        print 'here'
        DisplayBox.setText(QtGui.QFileDialog.getOpenFileName(directory=QtCore.QString(DAT_DIR),filter=QtCore.QString(file_types[0])))
        print 'here2'
        self.SubtaskTable.setRowCount(0)
        self.SubtaskTable.setColumnCount(0)
        print 'here3'
        #self.SubtaskTable.setColumnCount(0)
        self.temp_dir = None #tempfile.mkdtemp()
        self.plot_file = None #os.path.join(self.temp_dir, 'plot.png')
        if self.graph_created == True:
            print 'here4'
            #self.pixmap.destroy()
            self.pixbox.destroy()
            self.label.destroy()
            print 'here5'
            self.pixmap = None
            self.pixbox = None
            self.label = None
            self.graph_created = False
        print 'here6'

    def write_temp_csv(self, subtasks, actions, targets=None):
        if hasattr(subtasks[0], 'ptar'):
            ptar_model = True
        else:
            ptar_model = False
        default_csv_name = os.path.basename(os.path.splitext(self.subtask_file)[0]) + '.csv'
        self.default_csv = default_csv_name
        self.csv_file = os.path.join(self.temp_dir, 'results.csv')
        self.SubtaskTable.csv_file = self.csv_file
        self.SubtaskTable.default_csv = self.default_csv
        headers = []
        if ptar_model:
            target_headers = []
            ttl_dict = {}
            ttl_in_dict = {}
            for ttl_target in targets.keys():
                ttl_name = "TTL_Target {}".format(ttl_target)
                ttl_in_name = "TTL_In_Target {}".format(ttl_target)
                target_headers += [ttl_name, ttl_in_name]
                val = float(targets[ttl_target]['value'])
                ttl_dict[ttl_name] = val
                if (val > 0) and (val < subtasks[0].predint):
                    ttl_in_dict[ttl_name] = '1'
                else:
                    ttl_in_dict[ttl_name] = '0'
            self.ttl_dict = ttl_dict
            self.ttl_in_dict = ttl_in_dict
            raw_headers = ['Subtask'] + actions + ['Start', 'End', 'Duration', 'PTAR', 'TTI', 'TTL_TRAPS'] + target_headers
        else:
            raw_headers = ['Subtask'] + actions + ['Start', 'End', 'Duration','Transition', 'Dwell']
        for h in raw_headers:
            headers.append(h)
        with open(self.csv_file, "wb") as f:
            writer = csv.DictWriter(f, fieldnames=headers)
            writer.writeheader()
            for subtask in subtasks:
                row_dict = {}
                row_dict[headers[0]] = subtask.name
                i = 1
                for action in actions:
                    if action in subtask.actions:
                        was_performed = 1
                    else:
                        was_performed = 0
                    row_dict[headers[i]] = was_performed
                    i += 1
                row_dict[headers[i]] = format(float(subtask.start_ts), '0.6f')
                row_dict[headers[i+1]] = format(float(subtask.end_ts), '0.6f')
                row_dict[headers[i+2]] = format(float(subtask.end_ts - subtask.start_ts), '0.6f')
                if ptar_model:
                    row_dict[headers[i+3]] = format(float(subtask.ptar), '0.6f')
                    row_dict[headers[i+4]] = format(float(subtask.tti), '0.6f')
                    row_dict[headers[i+5]] = format(float(subtask.ttl_traps), '0.6f')
                    j = 0
                    for ttl_name in ttl_dict.keys():
                        row_dict[headers[i+j+6]] = ttl_dict[ttl_name]
                        row_dict[headers[i+j+7]] = ttl_in_dict[ttl_name]
                        j += 2
                else:
                    row_dict[headers[i+3]] = format(float(subtask.transition), '0.6f')
                    row_dict[headers[i+4]] = format(float((subtask.dwell)), '0.6f')
                writer.writerow(row_dict)

    def make_vert_labels(self, n_rows):
        vert_labs = []
        start_c = 65
        for i in range(n_rows):
            if (start_c + i) > 90:
                label = chr(start_c) + chr(start_c + i - (90 - 65) - 1)
                vert_labs.append(label)
            else:
                vert_labs.append(chr(start_c + i))
        return vert_labs

    def populate_table(self, subtasks, actions, targets=None):
        if hasattr(subtasks[0], 'ptar'):
            ptar_model = True
        else:
            ptar_model = False
        self.SubtaskTable.setRowCount(0)
        i = 0
        headers = []
        if ptar_model:
            target_headers = []
            ttl_dict = {}
            ttl_in_dict = {}
            for ttl_target in targets.keys():
                ttl_name = "TTL_Target {}".format(ttl_target)
                ttl_in_name = "TTL_In_Target {}".format(ttl_target)
                target_headers += [ttl_name, ttl_in_name]
                print targets[ttl_target]
                val = float(targets[ttl_target]['value'])
                ttl_dict[ttl_name] = val
                if (val > 0) and (val < subtasks[0].predint):
                    ttl_in_dict[ttl_name] = '1'
                else:
                    ttl_in_dict[ttl_name] = '0'
            raw_headers = ['Subtask'] + actions + ['Start', 'End', 'Duration', 'PTAR', 'TTI', 'TTL_TRAPS'] + target_headers
        else:
            raw_headers = ['Subtask'] + actions + ['Start', 'End', 'Duration','Transition', 'Dwell']
        for h in raw_headers:
            headers.append(h)
        #self.SubtaskTable.setRowCount(len(subtasks))
        #print "subtask length " + str(len(subtasks))
        #print self.temp_dir
        self.SubtaskTable.setColumnCount(len(raw_headers))#change this
        for subtask in subtasks:
            self.SubtaskTable.insertRow(i)
            self.SubtaskTable.setItem(i, 0, QtGui.QTableWidgetItem(subtask.name))
            j = 1
            for action in actions:
                if action in subtask.actions:
                    was_performed = 1
                else:
                    was_performed = 0
                self.SubtaskTable.setItem(i, j, QtGui.QTableWidgetItem(str(was_performed)))
                j += 1
                #print j
            self.SubtaskTable.setItem(i, j, QtGui.QTableWidgetItem(format(float(subtask.start_ts), '0.6f')))
            self.SubtaskTable.setItem(i, j+1, QtGui.QTableWidgetItem(format(float(subtask.end_ts), '0.6f')))
            self.SubtaskTable.setItem(i, j+2, QtGui.QTableWidgetItem(format(float(subtask.end_ts - subtask.start_ts), '0.6f')))
            if ptar_model:
                self.SubtaskTable.setItem(i, j+3, QtGui.QTableWidgetItem(format(float(subtask.ptar), '0.6f')))
                self.SubtaskTable.setItem(i, j+4, QtGui.QTableWidgetItem(format(float(subtask.tti), '0.6f')))
                self.SubtaskTable.setItem(i, j+5, QtGui.QTableWidgetItem(format(float(subtask.ttl_traps), '0.6f')))
                k = 0
                for ttl_name in ttl_dict.keys():
                    self.SubtaskTable.setItem(i, j+k+6, QtGui.QTableWidgetItem(str(ttl_dict[ttl_name])))
                    self.SubtaskTable.setItem(i, j+k+7, QtGui.QTableWidgetItem(str(ttl_in_dict[ttl_name])))
                    k += 2
            else:
                self.SubtaskTable.setItem(i, j+3, QtGui.QTableWidgetItem(format(float(subtask.transition), '0.6f')))
                self.SubtaskTable.setItem(i, j+4, QtGui.QTableWidgetItem(format(float(subtask.dwell), '0.6f')))
            i+= 1
        self.SubtaskTable.setHorizontalHeaderLabels(headers)
        self.SubtaskTable.setVerticalHeaderLabels(self.make_vert_labels(i))
        self.SubtaskTable.horizontalHeader().setResizeMode(QtGui.QHeaderView.ResizeToContents)
        self.SubtaskTable.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)

    def valid_selections(self):
        valid = True
        if self.subtask_file == '':
            self.display_error("No subtask file selected! Select valid subtask file before running!")
            valid = False
        if self.model_file == '':
            self.display_error("No model file selected! Select valid model file before running!")
            valid = False
        if self.config_file == '':
            self.display_error("No config file selected! Select valid config file before running!")
            valid = False
        #if self.model_def_file == '':
        #    self.display_error("No model definition file selected! Select valid model definition file before running!")
        #    valid = False
        if valid:
            if not os.path.isfile(self.subtask_file):
                self.display_error("Subtask file does not exist!")
                valid = False
            if not os.path.isfile(self.model_file):
                self.display_error("Model file does not exist!")
                valid = False
            if not os.path.isfile(self.config_file):
                self.display_error("Config file does not exist!")
                valid = False
            '''if not os.path.isfile(self.model_def_file):
                self.display_error("Model Defs file does not exist!")
                valid = False'''
        return valid

    def show_plot(self):
        targets = None
        if self.graph_created:
            print "loading existing graph"
            print self.plot_file
            #self.pixbox.destroy()
            #self.pixmap.destroy()
            #self.label.destroy()
            #self.scrollArea.destroy()
            #self.plot_file = os.path.join(self.temp_dir, 'plot.png')
        else:
            self.subtask_file = str(self.SubtaskSelectBox.text())
            self.model_file = str(self.ModelSelectBox.text())
            self.config_file = str(self.ConfigSelectBox.text())
            #self.model_def_file = str(self.DefSelectBox.text())
            if not self.valid_selections():
                return False
            '''
            self.model_file = "models/regression_model.py"
            self.subtask_file = "subtasks/subtask_example_nav.csv"
            self.config_file = "configs/test_config.json"
            self.model_def_file = 'model_defs.csv'''
            self.load_model_file(self.model_file)
            self.temp_dir = tempfile.mkdtemp()
            self.plot_file = os.path.join(self.temp_dir, 'plot.png')
            try:
                actions, subtasks = dat.load_subtasks(self.subtask_file)
            except Exception as inst:
                inst2 = copy.deepcopy(inst)
                self.log_error(inst)
                self.display_error("Invalid Subtask File!", inst2)
                return False
            try:
                flag_data = dat.load_config(self.config_file)
                if len(flag_data) == 2:
                    colors, flags = flag_data
                elif len(flag_data) == 3:
                    colors, flags, targets = flag_data
            except Exception as inst:
                inst2 = copy.deepcopy(inst)
                self.log_error(inst)
                self.display_error("invalid Flag File!", inst2)
                return False
            try:
                vm = dat.load_vector_model(self.model_logic_file)

            except Exception as inst:
                inst2 = copy.deepcopy(inst)
                self.log_error(inst)
                self.display_error("Invalid Model File!", inst2)
                return False
            try:
                my_model = vm.VectorModel(subtasks, self.model_def_file)
            except Exception as inst:
                inst2 = copy.deepcopy(inst)
                self.log_error(inst)
                self.display_error("Invalid Model File!", inst2)
                return False
            my_model.run()
            try:
                warm_streaks = dat.get_streaks(subtasks, 'warm', flags)
                hot_streaks = dat.get_streaks(subtasks, 'hot', flags)
                streaks = hot_streaks + warm_streaks
            except:
                self.display_error("Invalid Flag File for Model!")
            #print actions
            #print subtasks
            #print streaks
            #print colors

            '''self.canvas = FigureCanvas(fig)
            #self.canvas.setParent(self.PltWidget)
            if not self.graph_created:
                self.pltlayout.addWidget(self.canvas)
                self.graph_created = True
            self.canvas.draw()'''
            if targets is not None:
                self.write_temp_csv(subtasks, actions, targets=targets)
                self.populate_table(subtasks, actions, targets=targets)
                fig = dat.make_plot(actions, subtasks, streaks, colors, self.plot_file, ttl_targets=self.ttl_dict, ttl_in_targets=self.ttl_in_dict)
            else:
                self.populate_table(subtasks, actions)
                self.write_temp_csv(subtasks, actions)
                fig = dat.make_plot(actions, subtasks, streaks, colors, self.plot_file)
            if isinstance(fig, basestring):
                if fig == "badsubtaskfile":
                    self.display_error("Invalid Subtask File!")

            self.pixbox = QtGui.QWidget()
            self.pixmap = QtGui.QPixmap(os.path.join(self.plot_file))
            self.pixbox.setWindowTitle("plot")
            default_name = os.path.basename(os.path.splitext(self.subtask_file)[0])
            self.label = CustomLabel(self.pixbox, default_name=default_name)
            self.label.plot_file = self.plot_file
            self.label.setPixmap(self.pixmap)
            self.scrollArea = QtGui.QScrollArea()
            self.scrollArea.setBackgroundRole(QtGui.QPalette.Dark)
            self.scrollArea.setWidget(self.label)
            self.scrollArea.resize(self.pixmap.width(), self.pixmap.height())
            self.scrollArea.setWindowTitle("Plot")
        self.scrollArea.show()
        #self.pixbox.resize(self.pixmap.width(),self.pixmap.height())
        #self.pixbox.show()

        #self.SubtaskTable.setFocus()
        #self.SubtaskTable.setFocusPolicy(QtCore.Qt.StrongFocus)
        #self.SubtaskTable.setEnabled(True)
        #self.SubtaskTable.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
        self.graph_created = True

def main():
    app = QtGui.QApplication(sys.argv)
    form = DatApp()
    form.show()
    app.exec_()

if __name__=="__main__":
    main()
