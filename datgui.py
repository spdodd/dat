# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'datgui.ui'
#
# Created: Fri Apr 21 13:19:25 2017
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!


from PyQt4 import QtCore, QtGui
import shutil
import os
HOME_DIR = os.path.expanduser("~")
DAT_DIR = os.path.join(HOME_DIR, 'DAT')
GLOBAL_CSV = None
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class QCustomTableWidget(QtGui.QTableWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.csv_file = None
        self.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        saveAction = QtGui.QAction("Save CSV", self)
        saveAction.triggered.connect(self.save)
        self.addAction(saveAction)
        #self.menu = QtGui.QMenu(self)
        #self.saveaction = QtGui.QAction('Save Plot', self)
        #self.menu.addAction(self.saveaction)
        #self.menu.popup(QtGui.QCursor.pos())

    def save(self):
        print self.csv_file
        if self.csv_file:
            #default_name = os.path.splitext((os.path.split(self.csv_file)[-1]))[0]
            out_csv = QtGui.QFileDialog.getSaveFileName(directory=QtCore.QString(os.path.join(DAT_DIR, self.default_csv)),
                                                    filter=QtCore.QString('CSV file (*.csv)'))
            shutil.copy(self.csv_file, out_csv)

    def closeEvent(self, event):
        pass
        #print "blah blah " + str(event)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(1280, 460)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(1280, 460))
        MainWindow.setMaximumSize(QtCore.QSize(1280, 460))
        MainWindow.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(16, 12, 1241, 421))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.horizontalLayout_5 = QtGui.QHBoxLayout(self.widget)
        self.horizontalLayout_5.setMargin(0)
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.verticalLayout_5 = QtGui.QVBoxLayout()
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setContentsMargins(-1, -1, -1, 75)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.verticalLayout_4 = QtGui.QVBoxLayout()
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.label_5 = QtGui.QLabel(self.widget)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.verticalLayout_4.addWidget(self.label_5)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.ModelSelectBox = QtGui.QLineEdit(self.widget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.ModelSelectBox.sizePolicy().hasHeightForWidth())
        self.ModelSelectBox.setSizePolicy(sizePolicy)
        self.ModelSelectBox.setMinimumSize(QtCore.QSize(200, 0))
        self.ModelSelectBox.setObjectName(_fromUtf8("ModelSelectBox"))
        self.horizontalLayout.addWidget(self.ModelSelectBox)
        self.ModelSelectBtn = QtGui.QPushButton(self.widget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.ModelSelectBtn.sizePolicy().hasHeightForWidth())
        self.ModelSelectBtn.setSizePolicy(sizePolicy)
        self.ModelSelectBtn.setObjectName(_fromUtf8("ModelSelectBtn"))
        self.horizontalLayout.addWidget(self.ModelSelectBtn)
        self.verticalLayout_4.addLayout(self.horizontalLayout)
        self.verticalLayout_2.addLayout(self.verticalLayout_4)
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.label_4 = QtGui.QLabel(self.widget)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.verticalLayout_3.addWidget(self.label_4)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.SubtaskSelectBox = QtGui.QLineEdit(self.widget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.SubtaskSelectBox.sizePolicy().hasHeightForWidth())
        self.SubtaskSelectBox.setSizePolicy(sizePolicy)
        self.SubtaskSelectBox.setMinimumSize(QtCore.QSize(200, 0))
        self.SubtaskSelectBox.setObjectName(_fromUtf8("SubtaskSelectBox"))
        self.horizontalLayout_2.addWidget(self.SubtaskSelectBox)
        self.SubtaskSelectBtn = QtGui.QPushButton(self.widget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.SubtaskSelectBtn.sizePolicy().hasHeightForWidth())
        self.SubtaskSelectBtn.setSizePolicy(sizePolicy)
        self.SubtaskSelectBtn.setObjectName(_fromUtf8("SubtaskSelectBtn"))
        self.horizontalLayout_2.addWidget(self.SubtaskSelectBtn)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        self.verticalLayout_2.addLayout(self.verticalLayout_3)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label_3 = QtGui.QLabel(self.widget)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.verticalLayout.addWidget(self.label_3)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.ConfigSelectBox = QtGui.QLineEdit(self.widget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.ConfigSelectBox.sizePolicy().hasHeightForWidth())
        self.ConfigSelectBox.setSizePolicy(sizePolicy)
        self.ConfigSelectBox.setMinimumSize(QtCore.QSize(200, 0))
        self.ConfigSelectBox.setObjectName(_fromUtf8("ConfigSelectBox"))
        self.horizontalLayout_3.addWidget(self.ConfigSelectBox)
        self.ConfigSelectBtn = QtGui.QPushButton(self.widget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.ConfigSelectBtn.sizePolicy().hasHeightForWidth())
        self.ConfigSelectBtn.setSizePolicy(sizePolicy)
        self.ConfigSelectBtn.setObjectName(_fromUtf8("ConfigSelectBtn"))
        self.horizontalLayout_3.addWidget(self.ConfigSelectBtn)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.verticalLayout_5.addLayout(self.verticalLayout_2)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setContentsMargins(-1, 125, -1, -1)
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.RunBtn = QtGui.QPushButton(self.widget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.RunBtn.sizePolicy().hasHeightForWidth())
        self.RunBtn.setSizePolicy(sizePolicy)
        self.RunBtn.setMinimumSize(QtCore.QSize(0, 0))
        self.RunBtn.setObjectName(_fromUtf8("RunBtn"))
        self.horizontalLayout_4.addWidget(self.RunBtn)
        self.SaveBtn = QtGui.QPushButton(self.widget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.SaveBtn.sizePolicy().hasHeightForWidth())
        self.SaveBtn.setSizePolicy(sizePolicy)
        self.SaveBtn.setObjectName(_fromUtf8("SaveBtn"))
        self.horizontalLayout_4.addWidget(self.SaveBtn)
        self.LoadBtn = QtGui.QPushButton(self.widget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.LoadBtn.sizePolicy().hasHeightForWidth())
        self.LoadBtn.setSizePolicy(sizePolicy)
        self.LoadBtn.setObjectName(_fromUtf8("LoadBtn"))
        self.horizontalLayout_4.addWidget(self.LoadBtn)
        self.verticalLayout_5.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_5.addLayout(self.verticalLayout_5)
        self.SubtaskTable = QCustomTableWidget(self.widget)
        self.SubtaskTable.setMaximumSize(QtCore.QSize(1000, 16777215))
        self.SubtaskTable.setObjectName(_fromUtf8("SubtaskTable"))
        self.SubtaskTable.setColumnCount(0)
        self.SubtaskTable.setRowCount(0)
        self.horizontalLayout_5.addWidget(self.SubtaskTable)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.label_5.setText(_translate("MainWindow", "Vector Model File", None))
        self.ModelSelectBtn.setText(_translate("MainWindow", "Select", None))
        self.label_4.setText(_translate("MainWindow", "Subtask Structure File", None))
        self.SubtaskSelectBtn.setText(_translate("MainWindow", "Select", None))
        self.label_3.setText(_translate("MainWindow", "Flag File", None))
        self.ConfigSelectBtn.setText(_translate("MainWindow", "Select", None))
        self.RunBtn.setText(_translate("MainWindow", "Run", None))
        self.SaveBtn.setText(_translate("MainWindow", "Save DAT File", None))
        self.LoadBtn.setText(_translate("MainWindow", "Load DAT File", None))

