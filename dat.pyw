#!/usr/bin/env python2
from PyQt4 import QtGui, QtCore, Qt
import os
import sys
import datgui
import dat
import tempfile
import csv
import zipfile
import simplejson as json
from PyQt4.uic import loadUiType
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import (FigureCanvasQTAgg as FigureCanvas, NavigationToolbar2QT as NavigationToolBar)
reload(sys)

HOME_DIR = os.path.expanduser("~")
DAT_DIR = os.path.join(HOME_DIR, 'DAT')

sys.setdefaultencoding("utf-8")
class PictureBox(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        #self.setupUi(self)
    def closeEvent(self, event):
        pass
        #print "blah blah " + str(event)


class PlotBox(QtGui.QLabel):
    def _init__(self, img):
        super(PlotBox, self).__init__()
        self.setFrameStyle(QtGui.QFrame.StyledPanel)
        self.pixmap = QtGui.QPixmap(img)

    def paintEvnet(self, event):
        size = self.size()
        painter = QtGui.QPainter(self)
        point = QtCore.QPoint(0,0)
        scaledPix = self.pixmap.sclaed(size, Qt.KeepAspectRatio)
        point.setX((size.width - scaledPix.width())/2)
        point.setY((size.height() - scaledPix.width())/2)
        painter.drawPixMap(point, scaledPix)

class DatApp(QtGui.QMainWindow, datgui.Ui_MainWindow):
    def __init__(self, parent=None):
        super(DatApp, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle('AHEAD DAT')
        self.ModelSelectBtn.clicked.connect(lambda: self.select_file(self.ModelSelectBox, ['Model File (*.model)']))
        self.SubtaskSelectBtn.clicked.connect(lambda: self.select_file(self.SubtaskSelectBox, ['Subtask File (*.csv)']))
        self.ConfigSelectBtn.clicked.connect(lambda: self.select_file(self.ConfigSelectBox, ['Config File (*.json)']))
        #self.DefSelectBtn.clicked.connect(lambda: self.select_file(self.DefSelectBox, ['Model Definition File (*.csv)']))
        self.LoadBtn.clicked.connect(self.open_dat)
        self.RunBtn.clicked.connect(self.show_plot)
        self.SaveBtn.clicked.connect(self.save_file)
        self.graph_created = False
        self.temp_dir = None
        self.plot_file = None
        self.csv_file = None
        self.model_temp_dir = None

    def display_error(self, msg):
        error = QtGui.QErrorMessage()
        error.showMessage(msg)
        error.exec_()

    def load_model_file(self, model_file):
        self.model_temp_dir = tempfile.mkdtemp()
        with open(model_file, 'rb') as f:
            z = zipfile.ZipFile(f)
            for name in z.namelist():
                z.extract(name, self.model_temp_dir)
                if os.path.splitext(name)[1].lower() == '.py':
                    self.model_logic_file = os.path.join(self.model_temp_dir, name)
                elif os.path.splitext(name)[1].lower() == '.csv':
                    self.model_def_file = os.path.join(self.model_temp_dir, name)


    def load_dat(self, datfile):
        #print datfile
        dat_temp = tempfile.mkdtemp()
        with open(datfile, 'rb') as f:
            z = zipfile.ZipFile(f)
            for name in z.namelist():
                z.extract(name, dat_temp)
        with open(os.path.join(dat_temp, 'metadata.json'), 'rb') as f:
            metadata = json.load(f)
        self.model_file = os.path.join(dat_temp,metadata['model_file'])
        self.subtask_file = os.path.join(dat_temp,metadata['subtask_file'])
        #self.model_def_file = os.path.join(dat_temp,metadata['model_def_file'])
        self.plot_file = os.path.join(dat_temp,metadata['plot_file'])
        self.results_file = os.path.join(dat_temp,metadata['results_file'])
        self.config_file = os.path.join(dat_temp,metadata['config_file'])

        self.SubtaskSelectBox.setText(self.subtask_file)
        self.ModelSelectBox.setText(self.model_file)
        #self.DefSelectBox.setText(self.model_def_file)
        self.ConfigSelectBox.setText(self.config_file)

        self.repopulate_table()
        self.pixbox = PictureBox()
        self.pixmap = QtGui.QPixmap(os.path.join(self.plot_file))
        self.pixbox.setWindowTitle("sample plot")

        self.label = QtGui.QLabel(self.pixbox)
        self.label.setPixmap(self.pixmap)
        #self.label = PlotBox(self.plot_file)
        #self.pixmap = self.label.pixmap
        self.scrollArea = QtGui.QScrollArea()
        self.scrollArea.setBackgroundRole(QtGui.QPalette.Dark)
        self.scrollArea.setWidget(self.label)
        self.scrollArea.resize(self.pixmap.width(), self.pixmap.height())
        self.scrollArea.setWindowTitle("Plot")
        self.scrollArea.show()
        #pixbox = PlotBox(self.plot_file)


    def repopulate_table(self):
        with open(self.results_file, 'rb') as f:
            lines = f.readlines()
            i = 0
            for line in lines:
                if i == 0:
                    header = line.rstrip('\r\n').split(',')
                    self.SubtaskTable.setColumnCount(len(header))  # change this
                else:
                    self.SubtaskTable.insertRow(i - 1)
                    vals = line.rstrip('\r\n').split(',')
                    j = 0
                    for val in vals:
                        self.SubtaskTable.setItem(i - 1, j, QtGui.QTableWidgetItem(str(val)))
                        j += 1
                i += 1
            self.SubtaskTable.setHorizontalHeaderLabels(header)
            #self.SubtaskTable.setRowCount(i)
            self.SubtaskTable.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)

    def open_dat(self):
        datfile = QtGui.QFileDialog.getOpenFileName(directory=QtCore.QString(DAT_DIR),filter=QtCore.QString('DAT file (*.dat)'))
        self.load_dat(datfile)

    def save_file(self):
        if not self.graph_created:
            self.display_error("No Data Generated! Please run model before saving.")
            return False
        out_file = str(QtGui.QFileDialog.getSaveFileName(self, 'Save File', directory=QtCore.QString(DAT_DIR),filter='DAT File (*.dat)'))
        metadata = {'model_file':os.path.basename(self.model_file), 'subtask_file':os.path.basename(self.subtask_file),
                    'config_file':os.path.basename(self.config_file),'results_file':os.path.basename(self.csv_file),'plot_file':os.path.basename(self.plot_file)}
        metadata_file = os.path.join(self.temp_dir, 'metadata.json')
        with open(metadata_file, 'w') as f:
            json.dump(metadata, f)
        with zipfile.ZipFile(out_file, 'w') as zf:
            zf.write(self.plot_file, os.path.basename(self.plot_file))
            zf.write(self.csv_file, os.path.basename(self.csv_file))
            #zf.write(self.model_def_file, os.path.basename(self.model_def_file))
            zf.write(self.subtask_file, os.path.basename(self.subtask_file))
            zf.write(self.config_file, os.path.basename(self.config_file))
            zf.write(self.model_file, os.path.basename(self.model_file))
            zf.write(metadata_file, os.path.basename(metadata_file))

    def select_file(self, DisplayBox, file_types):
        #file_dialog = QtGui.QFileDialog(self)
        #file_dialog.setNameFilters(file_types)
        #file_dialog.selectNameFilter(file_types[0])
        #DisplayBox.setText(file_dialog.getOpenFileName())
        DisplayBox.setText(QtGui.QFileDialog.getOpenFileName(directory=QtCore.QString(DAT_DIR),filter=QtCore.QString(file_types[0])))
        self.SubtaskTable.setRowCount(0)
        self.SubtaskTable.setColumnCount(0)
        self.temp_dir = tempfile.mkdtemp()
        self.plot_file = os.path.join(self.temp_dir, 'plot.png')
        self.graph_created = False

    def write_temp_csv(self, subtasks, actions):
        self.csv_file = os.path.join(self.temp_dir, "results.csv")
        headers = []
        raw_headers = ['Subtask'] + actions + ['Start', 'End', 'Transition', 'Dwell']
        for h in raw_headers:
            headers.append(h.title())
        with open(self.csv_file, "wb") as f:
            writer = csv.DictWriter(f, fieldnames=headers)
            writer.writeheader()
            for subtask in subtasks:
                row_dict = {}
                row_dict[headers[0]] = subtask.name
                i = 1
                for action in actions:
                    if action in subtask.actions:
                        was_performed = 1
                    else:
                        was_performed = 0
                    row_dict[headers[i]] = was_performed
                    i += 1
                row_dict[headers[i]] = str(subtask.start_ts)
                row_dict[headers[i+1]] = str(subtask.end_ts)
                row_dict[headers[i+2]] = str(subtask.transition)
                row_dict[headers[i+3]] = str(subtask.dwell).rstrip('\n\r')
                writer.writerow(row_dict)

    def populate_table(self, subtasks, actions):
        self.SubtaskTable.setRowCount(0)
        i = 0
        headers = []
        raw_headers = ['Subtask'] + actions + ['Start', 'End', 'Transition', 'Dwell']
        for h in raw_headers:
            headers.append(h.title())
        #self.SubtaskTable.setRowCount(len(subtasks))
        #print "subtask length " + str(len(subtasks))
        #print self.temp_dir
        self.SubtaskTable.setColumnCount(len(actions) + 5)#change this
        for subtask in subtasks:
            self.SubtaskTable.insertRow(i)
            j = 1
            self.SubtaskTable.setItem(i, 0, QtGui.QTableWidgetItem(subtask.name))
            for action in actions:
                if action in subtask.actions:
                    was_performed = 1
                else:
                    was_performed = 0
                self.SubtaskTable.setItem(i, j, QtGui.QTableWidgetItem(str(was_performed)))
                j += 1
                #print j
            self.SubtaskTable.setItem(i,j,QtGui.QTableWidgetItem(str(subtask.start_ts)))
            self.SubtaskTable.setItem(i,j+1,QtGui.QTableWidgetItem(str(subtask.end_ts)))
            self.SubtaskTable.setItem(i,j+2, QtGui.QTableWidgetItem(str(subtask.transition)))
            self.SubtaskTable.setItem(i, j + 3, QtGui.QTableWidgetItem(str(subtask.dwell)))
            i+= 1
        self.SubtaskTable.setHorizontalHeaderLabels(headers)
        self.SubtaskTable.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)

    def valid_selections(self):
        valid = True
        if self.subtask_file == '':
            self.display_error("No subtask file selected! Select valid subtask file before running!")
            valid = False
        if self.model_file == '':
            self.display_error("No model file selected! Select valid model file before running!")
            valid = False
        if self.config_file == '':
            self.display_error("No config file selected! Select valid config file before running!")
            valid = False
        #if self.model_def_file == '':
        #    self.display_error("No model definition file selected! Select valid model definition file before running!")
        #    valid = False
        if valid:
            if not os.path.isfile(self.subtask_file):
                self.display_error("Subtask file does not exist!")
                valid = False
            if not os.path.isfile(self.model_file):
                self.display_error("Model file does not exist!")
                valid = False
            if not os.path.isfile(self.config_file):
                self.display_error("Config file does not exist!")
                valid = False
            '''if not os.path.isfile(self.model_def_file):
                self.display_error("Model Defs file does not exist!")
                valid = False'''
        return valid

    def show_plot(self):
        self.subtask_file = str(self.SubtaskSelectBox.text())
        self.model_file = str(self.ModelSelectBox.text())
        self.config_file = str(self.ConfigSelectBox.text())
        #self.model_def_file = str(self.DefSelectBox.text())
        if not self.valid_selections():
            return False
        '''
        self.model_file = "models/regression_model.py"
        self.subtask_file = "subtasks/subtask_example_nav.csv"
        self.config_file = "configs/test_config.json"
        self.model_def_file = 'model_defs.csv'''
        self.load_model_file(self.model_file)
        self.temp_dir = tempfile.mkdtemp()
        self.plot_file = os.path.join(self.temp_dir, 'plot.png')
        try:
            actions, subtasks = dat.load_subtasks(self.subtask_file)
        except:
            self.display_error("Invalid Subtask File!")
            return False
        try:
            colors, flags = dat.load_config(self.config_file)
        except:
            self.display_error("invalid Subtask File!")
            return False
        try:
            vm = dat.load_vector_model(self.model_logic_file)

        except:
            self.display_error("Invalid Model File!")
            return False
        try:
            my_model = vm.VectorModel(subtasks, self.model_def_file)
        except:
            self.display_error("Invalid Model File!")
            return False
        my_model.run()
        warm_streaks = dat.get_streaks(subtasks, 'warm', flags)
        hot_streaks = dat.get_streaks(subtasks, 'hot', flags)
        streaks = hot_streaks + warm_streaks
        #print actions
        #print subtasks
        #print streaks
        #print colors

        fig = dat.make_plot(actions, subtasks, streaks, colors, self.plot_file)
        if isinstance(fig, basestring):
            if fig == "badsubtaskfile":
                self.display_error("Invalid Subtask File!")
        '''self.canvas = FigureCanvas(fig)
        #self.canvas.setParent(self.PltWidget)
        if not self.graph_created:
            self.pltlayout.addWidget(self.canvas)
            self.graph_created = True
        self.canvas.draw()'''
        self.pixbox = PictureBox()
        self.pixmap = QtGui.QPixmap(os.path.join(self.plot_file))
        self.pixbox.setWindowTitle("sample plot")
        self.label = QtGui.QLabel(self.pixbox)
        self.label.setPixmap(self.pixmap)
        self.scrollArea = QtGui.QScrollArea()
        self.scrollArea.setBackgroundRole(QtGui.QPalette.Dark)
        self.scrollArea.setWidget(self.label)
        self.scrollArea.resize(self.pixmap.width(), self.pixmap.height())
        self.scrollArea.setWindowTitle("Plot")
        self.scrollArea.show()
        #self.pixbox.resize(self.pixmap.width(),self.pixmap.height())
        #self.pixbox.show()
        self.populate_table(subtasks, actions)
        self.write_temp_csv(subtasks, actions)
        #self.SubtaskTable.setFocus()
        #self.SubtaskTable.setFocusPolicy(QtCore.Qt.StrongFocus)
        #self.SubtaskTable.setEnabled(True)
        #self.SubtaskTable.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
        self.graph_created = True

def main():
    app = QtGui.QApplication(sys.argv)
    form = DatApp()
    form.show()
    app.exec_()

if __name__=="__main__":
    main()
