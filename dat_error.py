# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\dat_error.ui'
#
# Created: Tue Jun 20 12:34:00 2017
#      by: PyQt4 UI code generator 4.10
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import win32clipboard

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(452, 355)
        Dialog.setMinimumSize(QtCore.QSize(452, 355))
        Dialog.setMaximumSize(QtCore.QSize(452, 355))
        self.copyBtn = QtGui.QPushButton(Dialog)
        self.copyBtn.setGeometry(QtCore.QRect(178, 320, 95, 25))
        self.copyBtn.setMinimumSize(QtCore.QSize(0, 25))
        self.copyBtn.setMaximumSize(QtCore.QSize(100, 25))
        self.copyBtn.setObjectName(_fromUtf8("copyBtn"))
        self.copyBtn.clicked.connect(self.copytoclip)
        self.errorText = QtGui.QTextEdit(Dialog)
        self.errorText.setGeometry(QtCore.QRect(10, 21, 431, 281))
        self.errorText.setObjectName(_fromUtf8("errorText"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Error!", None))
        self.copyBtn.setText(_translate("Dialog", "Copy To Clipboard", None))

    def copytoclip(self):
        win32clipboard.OpenClipboard()
        win32clipboard.EmptyClipboard()
        win32clipboard.SetClipboardText(str(self.errorText.toPlainText()))
        win32clipboard.CloseClipboard()

class datError(QtGui.QDialog, Ui_Dialog):
    def __init__(self,parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.setupUi(self)